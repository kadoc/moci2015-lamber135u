package eu.telecomnancy.observateur;

import Adapter.TemperaturAdapter;

public interface Observer {
	public void update(TemperaturAdapter O);
}
