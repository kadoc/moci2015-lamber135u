package eu.telecomnancy.observateur;

public interface Observee {
	public void attach(Observer O);
	public void detach();
	public void notifyView();
	
}
