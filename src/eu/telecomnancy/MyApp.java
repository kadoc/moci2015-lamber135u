package eu.telecomnancy;

import Adapter.TemperaturAdapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class MyApp {
	
	public static void main(String[] args) {
        ISensor sensor = new TemperaturAdapter();
        new ConsoleUI(sensor);
    }
}
