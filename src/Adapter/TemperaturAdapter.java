package Adapter;

import eu.telecomnancy.observateur.Observee;
import eu.telecomnancy.observateur.Observer;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperaturAdapter implements ISensor, Observee{
	public LegacyTemperatureSensor mySensor = new LegacyTemperatureSensor();
	private Observer view;

	@Override
	public void on() {
		if (!mySensor.getStatus()){
			mySensor.onOff();
		}
		
	}

	@Override
	public void off() {
		if (mySensor.getStatus()){
			mySensor.onOff();
		}		
	}

	@Override
	public boolean getStatus() {
		return mySensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		mySensor.onOff();
		mySensor.onOff();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return mySensor.getTemperature();
	}

	@Override
	public void attach(Observer O) {
		this.view=O;
		
	}

	@Override
	public void detach() {
		this.view=null;
		
	}

	@Override
	public void notifyView() {
		view.update(this);
		
	}
	
	
	
}
